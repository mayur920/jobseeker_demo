from django.contrib import admin
from demo_app.models import Basic,Location,Profile,Work,Highlight,Volunteer,Education,Course,Award,Publication,Skill,Keyword,Language,Interest,Reference,Jobseeker

# register your models here.

admin.site.register(Jobseeker)


class BasicAdmin(admin.ModelAdmin):
	list_display = ('name','location','email')
	list_filter = ('name','location', 'profile', 'email')
	search_fields = ('location', 'email')

admin.site.register(Basic, BasicAdmin)


class LocationAdmin(admin.ModelAdmin):
	list_display = ('address','city','country_code')
	list_filter = ('address','city', 'country_code')
	search_fields = ('city', 'country_code')

admin.site.register(Location, LocationAdmin)

class ProfileAdmin(admin.ModelAdmin):
	list_display = ('network','username','url')
	list_filter = ('network','username', 'url')
	search_fields = ('network', 'url')

admin.site.register(Profile, ProfileAdmin)

class WorkAdmin(admin.ModelAdmin):
	list_display = ('company','position','website')
	list_filter = ('company','position', 'website')
	search_fields = ('network', 'url')

admin.site.register(Work, WorkAdmin)

class HighlightAdmin(admin.ModelAdmin):
	list_display = ('description',)
	list_filter = ('description',)
	search_fields = ('description',)

admin.site.register(Highlight, HighlightAdmin)

class VolunteerAdmin(admin.ModelAdmin):
	list_display = ('organization','position','website')
	list_filter = ('highlight','organization','position','website')
	search_fields = ('organization','position')

admin.site.register(Volunteer, VolunteerAdmin)

class EducationAdmin(admin.ModelAdmin):
	list_display = ('institution','area','study_type')
	list_filter = ('institution','area','study_type')
	search_fields = ('institution','area')

admin.site.register(Education, EducationAdmin)

class CourseAdmin(admin.ModelAdmin):
	list_display = ('course_type',)
	list_filter = ('course_type',)
	search_fields = ('course_type',)

admin.site.register(Course, CourseAdmin)

class AwardAdmin(admin.ModelAdmin):
	list_display = ('title','date','awarder')
	list_filter = ('title','date','awarder')
	search_fields = ('title','awarder')

admin.site.register(Award, AwardAdmin)

class PublicationAdmin(admin.ModelAdmin):
	list_display = ('name','publisher','release_date','website')
	list_filter = ('name','publisher','release_date','website')
	search_fields = ('publisher','name')

admin.site.register(Publication, PublicationAdmin)

class SkillAdmin(admin.ModelAdmin):
	list_display = ('name','level')
	list_filter = ('keyword','name','level')
	search_fields = ('name','level')

admin.site.register(Skill, SkillAdmin)

class KeywordAdmin(admin.ModelAdmin):
	list_display = ('keyword_name','keyword_two','keyword_three')
	list_filter = ('keyword_name','keyword_two','keyword_three')
	search_fields = ('keyword_name','keyword_two','keyword_three')

admin.site.register(Keyword, KeywordAdmin)

class LanguageAdmin(admin.ModelAdmin):
	list_display = ('name','level')
	list_filter = ('name','level')
	search_fields = ('name','level')

admin.site.register(Language, LanguageAdmin)

class ReferenceAdmin(admin.ModelAdmin):
	list_display = ('name','reference')
	list_filter = ('name','reference')
	search_fields = ('name','reference')

admin.site.register(Reference, ReferenceAdmin)

class InterestAdmin(admin.ModelAdmin):
	list_display = ('name',)
	list_filter = ('name',)
	search_fields = ('name',)

admin.site.register(Interest, InterestAdmin)
