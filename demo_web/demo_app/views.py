from django.shortcuts import render
from demo_app.models import Basic,Location,Profile,Work,Highlight,Volunteer,Education,Course,Award,Publication,Skill,Keyword,Language,Interest,Reference, Jobseeker
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import login,authenticate,logout
import json, datetime
from django.db import transaction
from django.core.management.base import CommandError


def create_jobseeker_profile_data(request):
    json_params = request.body.decode('utf-8')
    params = json.loads(json_params)
    print('params: ', params)
    basic = params.get('basics')
    work_data = params.get('work')
    highlight_data = params.get('highlights')
    volunteer_data = params.get('volunteer')
    education_data = params.get('education')
    course_data = params.get('courses')
    award_data = params.get('awards')
    publication_data = params.get('publications')
    skill_data = params.get('skills')
    keyword_data = params.get('keywords')
    language_data = params.get('languages')
    interest_data = params.get('interests')
    reference_data = params.get('references')

    if not basic.get('email'):
        return JsonResponse({'validation':'Enter email', 'status': False})

    with transaction.atomic():
        try:
            basic_obj = create_basic(basic)
            jobseeker_obj = Jobseeker.objects.create(basic=basic_obj)

            work_list = create_work(work_data)
            jobseeker_obj.work.add(*work_list)

            volunteer_list = create_volunteer(volunteer_data)
            jobseeker_obj.volunteer.add(*volunteer_list)

            education_list = create_education(education_data)
            jobseeker_obj.education.add(*education_list)

            award_list = create_award(award_data)
            jobseeker_obj.award.add(*award_list)

            publication_list = create_publication(publication_data)
            jobseeker_obj.publication.add(*publication_list)

            skill_list = create_skill(skill_data)
            jobseeker_obj.skill.add(*skill_list)

            language_list = create_language(language_data)
            jobseeker_obj.language.add(*language_list)

            interest_list = create_interest(interest_data)
            jobseeker_obj.interest.add(*interest_list)

            reference_list = create_reference(reference_data)
            jobseeker_obj.reference.add(*reference_list)

            jobseeker_obj.save()
        except Exception as e:
            print(e)
            return JsonResponse({'validation':'Something went wrong', 'status': False})

    return JsonResponse({'validation':'Successfully Created', 'status': True})

def create_basic(basic):
    basic_obj = Basic.objects.create(name=basic['name'],label=basic['label'],picture=basic['picture'],email=basic['email'].lower(),phone=basic['phone'],website=basic['website'],summary=basic['summary'])

    location_obj = create_location(basic['location'])
    basic_obj.location = location_obj
    profile_list = create_profile(basic['profiles'])
    basic_obj.profile.add(*profile_list)

    basic_obj.save()
    return basic_obj

def create_location(location):
    location_obj = Location.objects.create(address=location['address'], postal_code=location['postalCode'], city=location['city'], country_code=location['countryCode'], region=location['region'])

    return location_obj

def create_profile(profile_data):
    profile_list = []
    for profile in profile_data:
        profile_obj = Profile.objects.create(network=profile['network'],username=profile['username'],url=profile['url'])
        profile_list.append(profile_obj)
    return profile_list

def create_work(work_data):
    work_list = []
    for work in work_data:
        work_obj = Work.objects.create(company=work['company'],position=work['position'],website=work['website'],start_date=work['startDate'],end_date=work['endDate'],summary=work['summary'])
        work_list.append(work_obj)
        highlight_list = create_highlight(work['highlights'])
        work_obj.highlight.add(*highlight_list)
        work_obj.save()
    return work_list

def create_highlight(highlight_data):
    highlight_list = []
    for highlight in highlight_data:
        highlight_obj = Highlight.objects.create(description=highlight)
        highlight_list.append(highlight_obj)
    return highlight_list

def create_volunteer(volunteer_data):
    volunteer_list = []
    for volunteer in volunteer_data:
        volunteer_obj = Volunteer.objects.create(organization=volunteer['organization'],position=volunteer['position'],website=volunteer['website'],start_date=volunteer['startDate'],end_date=volunteer['endDate'],summary=volunteer['summary'])
        volunteer_list.append(volunteer_obj)
        highlight_list = create_highlight(volunteer['highlights'])
        volunteer_obj.highlight.add(*highlight_list)
        volunteer_obj.save()
    return volunteer_list

def create_education(education_data):
    education_list = []
    for education in education_data:
        education_obj = Education.objects.create(institution=education['institution'],area=education['area'],study_type=education['studyType'],start_date=education['startDate'],end_date=education['endDate'],gpa=education['gpa'])
        education_list.append(education_obj)
        course_list = create_course(education['courses'])
        education_obj.course.add(*course_list)
        education_obj.save()
    return education_list

def create_course(course_data):
    course_list = []
    for course in course_data:
        course_obj = Course.objects.create(course_type=course)
        course_list.append(course_obj)
    return course_list

def create_award(award_data):
    award_list = []
    for award in award_data:
        award_obj = Award.objects.create(title=award['title'],date=award['date'],awarder=award['awarder'],summary=award['summary'])
        award_list.append(award_obj)
    return award_list

def create_publication(publication_data):
    publication_list = []
    for publication in publication_data:
        publication_obj = Publication.objects.create(name=publication['name'],publisher=publication['publisher'],release_date=publication['releaseDate'],website=publication['website'],summary=publication['summary'])
        publication_list.append(publication_obj)
    return publication_list

def create_skill(skill_data):
    skill_list = []
    for skill in skill_data:
        skill_obj = Skill.objects.create(name=skill['name'],level=skill['level'])
        skill_list.append(skill_obj)
        keyword_list = create_keyword(skill['keywords'])
        skill_obj.keyword.add(*keyword_list)
        skill_obj.save()
    return skill_list

def create_keyword(keyword_data):
    keyword_list = []
    for keyword in keyword_data:
        keyword_obj = Keyword.objects.create(keyword_name=keyword,keyword_two=keyword,keyword_three=keyword)
        keyword_list.append(keyword_obj)
    return keyword_list

def create_language(language_data):
    language_list = []
    for language in language_data:
        language_obj = Language.objects.create(name=language['name'],level=language['level'])
        language_list.append(language_obj)
    return language_list

def create_interest(interest_data):
    interest_list =[]
    for interest in interest_data:
        interest_obj = Interest.objects.create(name=interest['name'])
        interest_list.append(interest_obj)
        keyword_list = create_keyword(interest['keywords'])
        interest_obj.keyword.add(*keyword_list)
        interest_obj.save()
    return interest_list

def create_reference(reference_data):
    reference_list = []
    for reference in reference_data:
        reference_obj = Reference.objects.create(name=reference['name'],reference=reference['reference'])
        reference_list.append(reference_obj)
    return reference_list
