from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
import os, re
import json
from demo_web.settings import BASE_DIR

# Create your models here.

class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class Jobseeker(BaseModel):
    basic = models.OneToOneField('Basic',on_delete=models.CASCADE)
    work = models.ManyToManyField('Work',blank=True)
    volunteer = models.ManyToManyField('Volunteer',blank=True)
    education = models.ManyToManyField('Education',blank=True)
    award = models.ManyToManyField('Award',blank=True)
    publication = models.ManyToManyField('Publication',blank=True)
    skill = models.ManyToManyField('Skill',blank=True)
    language = models.ManyToManyField('Language',blank=True)
    interest = models.ManyToManyField('Interest',blank=True)
    reference = models.ManyToManyField('Reference',blank=True)

    def __str__(self):
        return "{}".format(self.id)

class Basic(BaseModel):
    location = models.ForeignKey('Location', on_delete=models.CASCADE,null=True,blank=True)
    profile = models.ManyToManyField('Profile',blank=True)
    name = models.CharField(max_length=100,null=True,blank=True)
    label = models.CharField(max_length=50,null=True,blank=True)
    picture = models.ImageField(blank=True, null=True)
    email = models.EmailField(max_length=70,unique= True)
    phone = models.CharField(max_length=10,null=True,blank=True)
    website = models.CharField(max_length=30,null=True,blank=True)
    summary = models.CharField(max_length=100,null=True,blank=True)

    def __str__(self):
        return "{} {} {}".format(self.id, self.name, self.label)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['name'] = self.name if self.name else None
        result['label'] = self.label if self.label else None
        result['picture'] = self.picture if self.picture else None
        result['email'] = self.email if self.email else None
        result['phone'] = self.phone if self.phone else None
        result['website'] = self.website if self.website else None
        result['summary'] = self.summary if self.summary else None


class Location(BaseModel):
    address = models.CharField(max_length=100,null=True,blank=True)
    postal_code = models.CharField(max_length=30,null=True,blank=True)
    city = models.CharField(max_length=30,null=True,blank=True)
    country_code = models.CharField(max_length=10,null=True,blank=True)
    region = models.CharField(max_length=20,null=True,blank=True)

    def __str__(self):
        return "{} {} {}".format(self.address, self.postal_code, self.city)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['address'] = self.address if self.address else None
        result['postal_code'] = self.postal_code if self.postal_code else None
        result['city'] = self.city if self.city else None
        result['country_code'] = self.country_code if self.country_code else None
        result['region'] = self.region if self.region else None


class Profile(BaseModel):
    network = models.CharField(max_length=20,null=True,blank=True)
    username = models.CharField(max_length=50,null=True,blank=True)
    url = models.URLField(max_length=100,null=True,blank=True)

    def __str__(self):
        return "{} {} {}".format(self.network, self.username, self.url)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['network'] = self.network if self.network else None
        result['username'] = self.username if self.username else None
        result['url'] = self.url if self.url else None

class Work(BaseModel):
    highlight = models.ManyToManyField('Highlight',blank=True)
    company = models.CharField(max_length=100,null=True,blank=True)
    position = models.CharField(max_length=50,null=True,blank=True)
    website = models.CharField(max_length=100,null=True,blank=True)
    start_date = models.DateField()
    end_date = models.DateField()
    summary = models.CharField(max_length=100,null=True,blank=True)

    def __str__(self):
        return "{} {} {}".format(self.company, self.position, self.website)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['company'] = self.company if self.company else None
        result['position'] = self.position if self.position else None
        result['website'] = self.website if self.website else None
        result['start_date'] = self.start_date if self.start_date else None
        result['end_date'] = self.end_date if self.end_date else None
        result['summary'] = self.summary if self.summary else None


class Highlight(BaseModel):
    description = models.CharField(max_length=100,null=True,blank=True)

    def __str__(self):
        return "{} {}".format(self.id, self.description)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['description'] = self.description if self.description else None

class Volunteer(BaseModel):
    highlight = models.ManyToManyField('Highlight',blank=True)
    organization = models.CharField(max_length=100,null=True,blank=True)
    position = models.CharField(max_length=20,null=True,blank=True)
    website = models.CharField(max_length=50,null=True,blank=True)
    start_date = models.DateField()
    end_date = models.DateField()
    summary = models.CharField(max_length=100,null=True,blank=True)

    def __str__(self):
        return "{} {}".format(self.organization, self.position)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['organization'] = self.organization if self.organization else None
        result['position'] = self.position if self.position else None
        result['website'] = self.website if self.website else None
        result['start_date'] = self.start_date if self.start_date else None
        result['end_date'] = self.end_date if self.end_date else None
        result['summary'] = self.summary if self.summary else None



class Education(BaseModel):
    course = models.ManyToManyField('Course',blank=True)
    institution = models.CharField(max_length=50,null=True,blank=True)
    area = models.CharField(max_length=50,null=True,blank=True)
    study_type = models.CharField(max_length=50,null=True,blank=True)
    start_date = models.DateField()
    end_date = models.DateField()
    gpa =models.FloatField()

    def __str__(self):
        return "{} {} {}".format(self.institution, self.area, self.study_type)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['institution'] = self.institution if self.institution else None
        result['area'] = self.area if self.area else None
        result['study_type'] = self.study_type if self.study_type else None
        result['start_date'] = self.start_date if self.start_date else None
        result['end_date'] = self.end_date if self.end_date else None
        result['gpa'] = self.gpa if self.gpa else None


class Course(BaseModel):
    course_type = models.CharField(max_length=50)

    def __str__(self):
        return "{}".format(self.course_type)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['course_type'] = self.course_type if self.course_type else None


class Award(BaseModel):
    title = models.CharField(max_length=20,null=True,blank=True)
    date = models.DateField()
    awarder = models.CharField(max_length=100,null=True,blank=True)
    summary = models.CharField(max_length=100,null=True,blank=True)

    def __str__(self):
        return "{} {} {}".format(self.title, self.date, self.awarder)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['title'] = self.title if self.title else None
        result['date'] = self.date if self.date else None
        result['awarder'] = self.awarder if self.awarder else None
        result['summary'] = self.summary if self.summary else None


class Publication(BaseModel):
    name = models.CharField(max_length=20,null=True,blank=True)
    publisher = models.CharField(max_length=50,null=True,blank=True)
    release_date = models.DateField()
    website = models.CharField(max_length=100,null=True,blank=True)
    summary = models.CharField(max_length=100,null=True,blank=True)

    def __str__(self):
        return "{} {} {}".format(self.name, self.publisher, self.website)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['name'] = self.name if self.name else None
        result['publisher'] = self.publisher if self.publisher else None
        result['release_date'] = self.release_date if self.release_date else None
        result['website'] = self.website if self.website else None
        result['summary'] = self.summary if self.summary else None


class Skill(BaseModel):
    keyword = models.ManyToManyField('Keyword',blank=True)
    name = models.CharField(max_length=50,null=True,blank=True)
    level = models.CharField(max_length=50,null=True,blank=True)

    def __str__(self):
        return "{} {}".format(self.name, self.level)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['name'] = self.name if self.name else None
        result['level'] = self.level if self.level else None

class Keyword(BaseModel):
    keyword_name = models.CharField(max_length=100,null=True,blank=True)
    keyword_two= models.CharField(max_length=100,null=True,blank=True)
    keyword_three = models.CharField(max_length=100,null=True,blank=True)

    def __str__(self):
        return "{} {} {}".format(self.keyword_name,self.keyword_two,self.keyword_three)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['keyword_name'] = self.keyword_name if self.keyword_name else None
        result['keyword_two'] = self.keyword_two if self.keyword_two else None
        result['keyword_three'] = self.keyword_three if self.keyword_three else None


class Language(BaseModel):
    name = models.CharField(max_length=50,null=True,blank=True)
    level = models.CharField(max_length=50,null=True,blank=True)

    def __str__(self):
        return "{} {}".format(self.name, self.level)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['name'] = self.name if self.name else None
        result['level'] = self.level if self.level else None

class Interest(BaseModel):
    keyword = models.ManyToManyField('Keyword',blank=True)
    name = models.CharField(max_length=50,null=True,blank=True)

    def __str__(self):
        return "{}".format(self.name)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['name'] = self.name if self.name else None

class Reference(BaseModel):
    name = models.CharField(max_length=50,null=True,blank=True)
    reference = models.CharField(max_length=50,null=True,blank=True)

    def __str__(self):
        return "{}".format(self.name)

    def get_json(self):
        result = {}
        result['id'] = self.id if self.id else None
        result['name'] = self.name if self.name else None
        result['reference'] = self.reference if self.reference else None




